import pyotp
import base64
import hashlib

def hotp_generator(secret_string: str) -> int:
    """Generate a single HASHLIB_SHA_512 encoded, One Time Password of 10 digits with a time step of 30 seconds.
    refer RFC 6238 and RFC 4226 for more information

    secret_string = secret string that will be used for generating HOTP.
    """
    secret_bytes = secret_string.encode('utf8')
    secret_hashed = base64.b32encode(secret_bytes).decode('utf8')
    hotp = int(pyotp.TOTP(secret_hashed, digits=10, digest=hashlib.sha512, interval=30).now())

    return hotp