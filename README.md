# Generate a single HASHLIB_SHA_512 encoded, Time-based One-Time Password of 10 digits with a time step of 30 seconds.

## The solution is using Pytop and hashlib with sha512.